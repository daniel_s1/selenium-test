# Selenium-Java-Allure Framework

A Maven framework in which to build Selenium tests written in Java with Allure reports of test results. 

## Getting Started

Copy the repo into your local machine.

### Run tests locally
Right click the feature file and select "Run" or "Debug" to start the test.

N.B. You must define the location of the chromedriver binary (Chrome is the default browser).  To do this, pass the location as an overriding property in VM options, such as: 
```
-Dselenium.driver.chrome.path=C:\selenium\chromedriver.exe
```

### Run tests through the commandline
As this project uses Maven, we can invoke the tests using Maven goals.

To run the test, use your CI or point Maven to the project and use the goals:
```
clean install site
```

### Defining the browser

By default, the project will default to ChromeLocal (running a local Chrome instance) if no browser is specified.

To express a specific browser type, at runtime or through VM options in your IDE, pass the following overriding property, such as the following:
```
-Dbrowser.type=FirefoxLocal
```

### Pointing to a Selenium Grid

To point your tests to a Selenium grid, at runtime or through VM options in your IDE, pass the following overriding property, such as the following:

```
-Dselenium.grid.url=http://localhost:4444/wd/hub
```

N.B. When running tests on a remote grid, you must specify a "Remote" type browser, e.g. ChromeRemote:

```
-Dbrowser.type=ChromeRemote
```


## Built With

* [Selenium](https://github.com/SeleniumHQ/selenium) - Browser automation framework
* [Maven](https://maven.apache.org/) - Dependency management
* [TestNG](https://github.com/cbeust/testng) - Testing framework
* [Allure](https://github.com/allure-framework) - Reporting framework

## Licence

This project is licensed under the MIT License.