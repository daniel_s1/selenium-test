package uk.ac.cam.automation.seleniumframework.file;

import org.testng.annotations.Test;
import uk.ac.cam.automation.seleniumframework.properties.PropertiesFileConfig;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

import java.io.IOException;

import static org.testng.Assert.assertEquals;

public class FileReaderTest {

    static {
        PropertyLoader.registerPropertiesFileConfig(new PropertiesFileConfig("properties/test.properties"));
    }

    @Test
    public void testReadFile() throws IOException {
        String s = FileReader.readFileFromResources("properties/test.properties");
        assertEquals("test.property=test1", s);
    }

}
