package uk.ac.cam.automation.seleniumframework.screenshots;

import io.qameta.allure.Allure;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import uk.ac.cam.automation.seleniumframework.driver.DriverManager;
import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class ScreenshotTaker {
    private static boolean isScreenshotOff = Boolean.parseBoolean(PropertyLoader.getProperty(CommonProperties.SCREENSHOT_OFF));
    private static boolean isDesktopGuiTest = Boolean.parseBoolean(PropertyLoader.getProperty(CommonProperties.DESKTOP_GUI_TEST));

    public static void attachScreenshot() {
        if (!isScreenshotOff) {
            Allure.getLifecycle().addAttachment(
                    LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MMM-yy_hh:mm:ss")),
                    "image/png",
                    "png",
                    getWebDriverScreenshot()
            );
        } else if (isDesktopGuiTest) {
            Allure.getLifecycle().addAttachment(
                    LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MMM-yy_hh:mm:ss")),
                    "image/png",
                    "png",
                    getRobotScreenshot()
            );
        }
    }

    private static byte[] getWebDriverScreenshot() {
        return (((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES));
    }

    private static byte[] getRobotScreenshot() {
        byte[] imageInByte = null;
        try {
            BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "png", baos);
            baos.flush();
            imageInByte = baos.toByteArray();
            baos.close();
        } catch (AWTException | IOException e) {
            e.printStackTrace();
        }
        return Objects.requireNonNull(imageInByte);
    }

}
