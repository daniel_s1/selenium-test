package uk.ac.cam.automation.seleniumframework.driver;

public enum BrowserType {

    ChromeLegacyRemote,
    ChromeLocal,
    ChromeLocalNoSandbox,
    ChromeRemote,
    ChromeMobileEmulationLocal,
    ChromeMobileEmulationRemote,
    ChromeEuropeanLocaleRemote,
    AppiumWindowsTenRemote,
    AppiumMobile,
    EdgeLocal,
    EdgeRemote,
    EdgeBrowserStackRemote,
    FirefoxLocal,
    FirefoxLocalDebug,
    FirefoxRemote,
    FirefoxEagerLoadRemote,
    FirefoxRemoteDebug,
    InternetExplorerLocal,
    InternetExplorerNativeEventsRemote,
    InternetExplorerRemote,
    OperaLocal,
    OperaRemote,
    SafariLocal,
    SafariRemote,
    SafariTechPreviewLocal,
    SafariTechPreviewRemote

}
