package uk.ac.cam.automation.seleniumframework.jsf.primefaces;

import org.openqa.selenium.By;
import uk.ac.cam.automation.seleniumframework.ValueNotFoundException;
import uk.ac.cam.automation.seleniumframework.site.Site;

public class PrimefacesSelectCheckboxMenu extends BasePrimefacesUtil {

    @Deprecated
    //TODO: This can be refactored in line with other PrimeFaces classes to avoid StaleElement exceptions thrown due to caches elements that cause havoc with JSF
    public static void select(String selectCheckboxMenuId, String... values) {
        Site.click(By.id(selectCheckboxMenuId));
        for (String value : values) {
            if (!clickElementInList(value, selectCheckboxMenuId, "ui-selectcheckboxmenu-items-wrapper")) {
                throw new ValueNotFoundException("No value of " + value + " found in selectone menu: " + selectCheckboxMenuId);
            }
        }
    }

}
