package uk.ac.cam.automation.seleniumframework.properties;

public class CommonProperties {

    public final static String BROWSER_TYPE = "browser.type";

    public final static String PROJECT_NAME = "project.name";

    public static final String SCREENSHOT_OFF = "screenshot.off";
    public static final String DESKTOP_GUI_TEST = "desktop.gui.test";

    public final static String SELENIUM_GRID_URL = "selenium.grid.url";
    public final static String SELENIUM_DRIVER_WAIT_TIMEOUT = "selenium.driver.wait.timeout";
    public final static String SELENIUM_DRIVER_PATH_CHROME = "selenium.driver.chrome.path";
    public final static String SELENIUM_DRIVER_PATH_FIREFOX = "selenium.driver.firefox.path";
    public final static String SELENIUM_DRIVER_PATH_OPERA = "selenium.driver.opera.path";
    public final static String SELENIUM_DRIVER_PATH_EDGE = "selenium.driver.edge.path";
    public final static String SELENIUM_DRIVER_PATH_IE = "selenium.driver.ie.path";

    public static final String MAIL_HOST = "mail.host";
    public static final String MAIL_RETRIEVAL_PROTOCOL = "mail.retrieval.protocol";
    public static final String MAIL_RETRIEVAL_PORT = "mail.retrieval.port";
    public static final String MAIL_TIMEOUT_IN_SECONDS = "mail.timeout.seconds";

}
