package uk.ac.cam.automation.seleniumframework.jsf.primefaces;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import uk.ac.cam.automation.seleniumframework.ValueNotFoundException;
import uk.ac.cam.automation.seleniumframework.site.Site;

public class PrimefacesAutocomplete extends BasePrimefacesUtil {

    public static void selectPartialMatch(String autocompleteId, String searchValue, String... matchValueContaining) {
        String xpathLocator;
        if (matchValueContaining.length > 0 ) {
                xpathLocator = containsXpathStringBuilder(matchValueContaining);
        } else {
            throw new ValueNotFoundException("No match criteria set for autocomplete.");
        }
        Site.webDriverWait().until(ExpectedConditions.elementToBeClickable(By.id(autocompleteId + "_input")));
        Site.enterTextBoxDetails(By.id(autocompleteId + "_input"), searchValue);
        Site.click(By.xpath("(//li[contains(@class,\"autocomplete\") and " + xpathLocator + " ]|//tr[contains(@class,\"autocomplete\")]//td[" + xpathLocator + "])[1]"));
        Site.webDriverWait().until(ExpectedConditions.invisibilityOfElementLocated(By.id(autocompleteId + "_panel")));
    }

    public static void selectExactMatch(String autocompleteId, String searchValue, String matchValueEquals) {
        Site.webDriverWait().until(ExpectedConditions.elementToBeClickable(By.id(autocompleteId + "_input")));
        Site.enterTextBoxDetails(By.id(autocompleteId + "_input"), searchValue);
        Site.webDriverWait(2).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(@class,'ui-autocomplete-query')][1]")));
        Site.click(By.xpath("(//li[contains(@class,\"autocomplete\") and normalize-space(string())=\"" + matchValueEquals + "\" ])[1]"));
        Site.webDriverWait().until(ExpectedConditions.invisibilityOfElementLocated(By.id(autocompleteId + "_panel")));
    }

    private static String containsXpathStringBuilder(String... containXpathStrings){
        StringBuilder text = new StringBuilder();
        if (containXpathStrings.length != 0) {
            for (String stext : containXpathStrings) {
                String cont = "contains(string(),\"" + stext + "\") ";
                if (text.length() > 0) {
                    text.append(" and ");
                }
                text.append(cont);
            }
            return text.toString();
        } else {
            throw new ValueNotFoundException("No partial match criteria was specified.  Make sure you have at least one String that is contained in the match");
        }
    }
}
