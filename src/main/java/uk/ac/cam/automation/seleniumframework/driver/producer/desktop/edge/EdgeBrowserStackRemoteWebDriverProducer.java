package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.edge;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import uk.ac.cam.automation.seleniumframework.driver.GridUtils;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;

public class EdgeBrowserStackRemoteWebDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.setCapability("os", "Windows");
        edgeOptions.setCapability("os_version", "10");
        edgeOptions.setCapability("browser", "Edge");
        edgeOptions.setCapability("browser_version", "17.0");
        edgeOptions.setCapability("browserstack.local", "false");
        edgeOptions.setCapability("browserstack.video", "false");
        edgeOptions.setCapability("browserstack.edge.enablePopups", "true");
        edgeOptions.setCapability("browserstack.geoLocation", "GB");
        edgeOptions.setCapability("resolution", "1280x1024");
        RemoteWebDriver remoteWebDriver = new RemoteWebDriver(GridUtils.getBrowserstackGridURL(), edgeOptions);
        remoteWebDriver.setFileDetector(new LocalFileDetector());
        return remoteWebDriver;
    }

}