package uk.ac.cam.automation.seleniumframework.email;

public class MailDeletionException extends RuntimeException {

    public MailDeletionException(String message) {
        super(message);
    }

    public MailDeletionException(String message, Throwable e) {
        super(message, e);
    }

}