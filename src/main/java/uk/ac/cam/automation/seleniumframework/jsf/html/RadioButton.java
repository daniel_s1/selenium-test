package uk.ac.cam.automation.seleniumframework.jsf.html;

import org.openqa.selenium.By;
import uk.ac.cam.automation.seleniumframework.site.Site;

public class RadioButton {

    public static void select(String radioButtonComponentId, String text) {
        Site.click(By.xpath("//*[contains(@id, \"" + radioButtonComponentId + "\")]/..//*[contains(text(), \"" + text + "\")]"));
    }

}
