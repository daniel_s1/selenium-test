package uk.ac.cam.automation.seleniumframework.properties;

public class AppiumMobileProperties {

    public final static String BROWSER_NAME = "appium.browser.name";
    public final static String DEVICE_NAME = "appium.device.name";
    public final static String PLATFORM_NAME = "appium.platform.name";
    public final static String PLATFORM_VERSION = "appium.platform.version";
    public final static String AUTOMATION_NAME = "appium.automation.name";

}
