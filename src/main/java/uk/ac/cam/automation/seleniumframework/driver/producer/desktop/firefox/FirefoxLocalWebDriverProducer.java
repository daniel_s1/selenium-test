package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.firefox;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import uk.ac.cam.automation.seleniumframework.driver.DriverCreationException;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;
import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

public class FirefoxLocalWebDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        if (System.getProperty("webdriver.gecko.driver") == null) {
            String fireFox = PropertyLoader.getProperty(CommonProperties.SELENIUM_DRIVER_PATH_FIREFOX);
            if (fireFox == null) {
                throw new DriverCreationException("You have specified Firefox Local as your browser but have not set the " + CommonProperties.SELENIUM_DRIVER_PATH_FIREFOX + " property either as a command line argument or in a registered properties file.");
            }

            System.setProperty("webdriver.gecko.driver", fireFox);
        }

        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setCapability(FirefoxDriver.MARIONETTE, true);
        firefoxOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        return new FirefoxDriver(firefoxOptions);
    }

}
