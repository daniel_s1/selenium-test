package uk.ac.cam.automation.seleniumframework.frames;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import uk.ac.cam.automation.seleniumframework.driver.DriverManager;
import uk.ac.cam.automation.seleniumframework.site.Site;

public class Frames {
    public static void switchToFrame(By locator) {
        Site.webDriverWait().until(ExpectedConditions.visibilityOfElementLocated(locator));
        DriverManager.getDriver().switchTo().frame(Site.findElement(locator));
    }

    public static void switchBackToParentFrame() {
        DriverManager.getDriver().switchTo().parentFrame();
    }

    public static void switchBackToDefaultFrame() {
        DriverManager.getDriver().switchTo().defaultContent();
    }

}
