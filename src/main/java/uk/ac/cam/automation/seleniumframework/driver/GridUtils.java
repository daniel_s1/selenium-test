package uk.ac.cam.automation.seleniumframework.driver;

import uk.ac.cam.automation.seleniumframework.log.Log;
import uk.ac.cam.automation.seleniumframework.properties.BrowserstackProperties;
import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

import java.net.MalformedURLException;
import java.net.URL;

public class GridUtils {

    public static URL getSeleniumGridURL() {

        String seleniumGridUrl = PropertyLoader.getProperty(CommonProperties.SELENIUM_GRID_URL);

        if (seleniumGridUrl == null) {
            throw new DriverCreationException("No Selenium grid url specified in the configuration.  Make sure to set the selenium.grid.url property.");
        }
        try {
            Log.Info("Loading driver with grid url of " + seleniumGridUrl);
            return new URL(seleniumGridUrl);
        } catch (MalformedURLException e) {
            throw new DriverCreationException("Unable create a seleniumframework grid url from: " + seleniumGridUrl, e);
        }
    }

    public static URL getBrowserstackGridURL() {

        String username = PropertyLoader.getProperty(BrowserstackProperties.BROWSERSTACK_USERNAME);
        String accessKey = PropertyLoader.getProperty(BrowserstackProperties.BROWSERSTACK_ACCESS_KEY);

        String browserstackUrl = "https://" + username + ":" + accessKey + "@hub-cloud.browserstack.com/wd/hub";

        if (username == null) {
            throw new DriverCreationException("Browserstack Error: No username specified");
        }
        if (accessKey == null) {
            throw new DriverCreationException("Browserstack Error: No access key specified");
        }
        try {
            Log.Info("Loading driver via Browserstack...");
            return new URL(browserstackUrl);
        } catch (MalformedURLException e) {
            throw new DriverCreationException("Unable create a driver for browserstack from: " + browserstackUrl, e);
        }
    }

}
