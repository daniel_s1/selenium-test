package uk.ac.cam.automation.seleniumframework.driver;

import org.openqa.selenium.WebDriver;
import uk.ac.cam.automation.seleniumframework.log.Log;
import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

public class DriverManager {
    private static final ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();

    static {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (webDriver.get() != null) {
                clearDriver();
            }
        }));
    }

    public static WebDriver getDriver() {
        WebDriver d = webDriver.get();

        if (d == null) {
            Log.Info("Driver needs to be created, creating one...");
            d = createDriver();
        }

        return d;
    }

    public static void clearDriver() {
        try {
            webDriver.get().quit();
        } catch (Exception e) {
            Log.Warn("Couldn't quit one of the locally created drivers because: " + e);
        }
        webDriver.set(null);
    }

    private static WebDriver createDriver() {
        String currentBrowser = PropertyLoader.getProperty(CommonProperties.BROWSER_TYPE);
        WebDriver d = new DriverFactory(currentBrowser).createInstance();
        webDriver.set(d);
        return d;
    }

}
