package uk.ac.cam.automation.seleniumframework.email.domain;

public class FailedRecipient {
    private final Recipient recipient;
    private final Throwable failedReason;

    public FailedRecipient(Recipient recipient, Throwable failedReason) {
        this.recipient = recipient;
        this.failedReason = failedReason;
    }

    public Recipient getRecipient() {
        return this.recipient;
    }

    public Throwable getFailedReason() {
        return this.failedReason;
    }
}

