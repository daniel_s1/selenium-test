package uk.ac.cam.automation.seleniumframework.html;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import uk.ac.cam.automation.seleniumframework.site.Site;

public class Table {

    public static void clickButtonByTextInTableWithExactMatch(String exactCellText, String buttonText) {
        //Prioritise td element, and fall back to th if not as this is an uncommon occurrence
        Site.webDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[string()=\"" + exactCellText + "\" or @value=\"" + exactCellText + "\"][1]")));
        Site.webDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[string()=\"" + buttonText + "\"][1]")));
        if (Site.isElementPresent(By.xpath("((//*[(string()=\"" + exactCellText + "\" or (./*[@value=\"" + exactCellText + "\"])) and self::td])[last()]//parent::tr//*[string()=\"" + buttonText + "\"])[1]"))) {
            Site.click(By.xpath("((//*[(string()=\"" + exactCellText + "\" or (./*[@value=\"" + exactCellText + "\"])) and self::td])[last()]//parent::tr//*[string()=\"" + buttonText + "\"])[1]"));
        } else {
            Site.click(By.xpath("((//*[(string()=\"" + exactCellText + "\" or (./*[@value=\"" + exactCellText + "\"])) and self::th])[last()]//parent::tr//*[string()=\"" + buttonText + "\"])[1]"));
        }
    }

    public static void clickButtonByTextInTableWithPartialMatch(String partialMatchText, String buttonText) {
        Site.webDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(string(),\"" + partialMatchText + "\") or contains(@value,\"" + partialMatchText + "\")][1]")));
        Site.webDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[string()=\"" + buttonText + "\"][1]")));
        if (Site.isElementPresent(By.xpath("((//*[(contains(string(),\"" + partialMatchText + "\") or (./*[contains(@value,\"" + partialMatchText + "\")])) and self::td])[last()]//parent::tr//*[string()=\"" + buttonText + "\"])[1]"))) {
            Site.click(By.xpath("((//*[(contains(string(),\"" + partialMatchText + "\") or (./*[contains(@value,\"" + partialMatchText + "\")])) and self::td])[last()]//parent::tr//*[string()=\"" + buttonText + "\"])[1]"));
        } else {
            Site.click(By.xpath("((//*[(contains(string(),\"" + partialMatchText + "\") or (./*[contains(@value,\"" + partialMatchText + "\")])) and self::th])[last()]//parent::tr//*[string()=\"" + buttonText + "\"])[1]"));
        }
    }

    public static void clickButtonByClassInTableWithExactMatch(String exactCellText, String buttonClass) {
        Site.webDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[string()=\"" + exactCellText + "\" or @value=\"" + exactCellText + "\"][1]")));
        Site.webDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@class,\"" + buttonClass + "\")][1]")));
        if (Site.isElementPresent(By.xpath("((//*[(string()=\"" + exactCellText + "\" or (./*[@value=\"" + exactCellText + "\"])) and self::td])[last()]//parent::tr//*[contains(@class,\"" + buttonClass + "\")])[1]"))) {
            Site.click(By.xpath("((//*[(string()=\"" + exactCellText + "\" or (./*[@value=\"" + exactCellText + "\"])) and self::td])[last()]//parent::tr//*[contains(@class,\"" + buttonClass + "\")])[1]"));
        } else {
            Site.click(By.xpath("((//*[(string()=\"" + exactCellText + "\" or (./*[@value=\"" + exactCellText + "\"])) and self::th])[last()]//parent::tr//*[contains(@class,\"" + buttonClass + "\")])[1]"));
        }
    }

    public static void clickButtonByClassInTableWithPartialMatch(String partialMatchText, String buttonClass) {
        Site.webDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(string(),\"" + partialMatchText + "\") or contains(@value,\"" + partialMatchText + "\")][1]")));
        Site.webDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@class,\"" + buttonClass + "\")][1]")));
        if (Site.isElementPresent(By.xpath("((//*[(contains(string(),\"" + partialMatchText + "\") or (./*[contains(@value,\"" + partialMatchText + "\")])) and self::td])[last()]//parent::tr//*[contains(@class,\"" + buttonClass + "\")])[1]"))) {
            Site.click(By.xpath("((//*[(contains(string(),\"" + partialMatchText + "\") or (./*[contains(@value,\"" + partialMatchText + "\")])) and self::td])[last()]//parent::tr//*[contains(@class,\"" + buttonClass + "\")])[1]"));
        } else {
            Site.click(By.xpath("((//*[(contains(string(),\"" + partialMatchText + "\") or (./*[contains(@value,\"" + partialMatchText + "\")])) and self::th])[last()]//parent::tr//*[contains(@class,\"" + buttonClass + "\")])[1]"));
        }
    }

}
