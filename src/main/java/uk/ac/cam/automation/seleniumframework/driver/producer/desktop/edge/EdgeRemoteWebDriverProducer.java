package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.edge;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import uk.ac.cam.automation.seleniumframework.driver.GridUtils;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;

public class EdgeRemoteWebDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        EdgeOptions edgeOptions = new EdgeOptions();
        RemoteWebDriver remoteWebDriver = new RemoteWebDriver(GridUtils.getSeleniumGridURL(), edgeOptions);
        remoteWebDriver.setFileDetector(new LocalFileDetector());
        return remoteWebDriver;
    }

}