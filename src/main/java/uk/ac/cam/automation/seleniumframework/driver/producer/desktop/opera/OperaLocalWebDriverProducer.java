package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.opera;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;
import uk.ac.cam.automation.seleniumframework.driver.DriverCreationException;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;
import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

public class OperaLocalWebDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        if (System.getProperty("webdriver.opera.driver") == null) {
            String operaDriver = PropertyLoader.getProperty(CommonProperties.SELENIUM_DRIVER_PATH_OPERA);
            if (operaDriver == null) {
                throw new DriverCreationException("You have specified Opera Local as your browser but have not set the " + CommonProperties.SELENIUM_DRIVER_PATH_OPERA + " property either as a command line argument or in a registered properties file.");
            }
            System.setProperty("webdriver.opera.driver", operaDriver);
        }
        return new OperaDriver();
    }
    
}

