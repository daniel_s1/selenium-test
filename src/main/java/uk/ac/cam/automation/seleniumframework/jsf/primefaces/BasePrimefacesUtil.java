package uk.ac.cam.automation.seleniumframework.jsf.primefaces;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import uk.ac.cam.automation.seleniumframework.site.Site;

import java.util.Arrays;
import java.util.List;

public class BasePrimefacesUtil {

    protected static boolean isMatch(String[] values, String listItemText, boolean exactMatch) {
        if (exactMatch) {
            return Arrays.stream(values).allMatch(listItemText::equals);
        } else {
            return Arrays.stream(values).allMatch(val -> val == null || listItemText.contains(val));
        }
    }

    @Deprecated
    protected static boolean clickElementInList(String value, String componentId, String wrapperClassName) {
        return clickElementInList(value, componentId, wrapperClassName, false);
    }

    @Deprecated
    protected static boolean clickElementInList(String[] values, String componentId, String wrapperClassName, boolean exactMatch) {
        Site.webDriverWait().until(ExpectedConditions.textToBePresentInElementLocated(By.id(componentId + "_panel"), values[0]));
        Site.webDriverWait().until(ExpectedConditions.elementToBeClickable(By.id(componentId + "_panel")));

        List<WebElement> searchResultsElements = getSearchElements(componentId, wrapperClassName);

        for (WebElement listItem : searchResultsElements) {
            if (isMatch(values, listItem.getText(), exactMatch)) {
                listItem.click();
                return true;
            }
        }

        return false;
    }

    private static List<WebElement> getSearchElements(String componentId, String wrapperClassName) {
        WebElement searchResults = Site.findElement(By.id(componentId + "_panel")).findElements(By.className(wrapperClassName)).stream().findFirst().get();

        List<WebElement> searchResultsElements = searchResults.findElements(By.tagName("li"));
        searchResultsElements.addAll(searchResults.findElements(By.tagName("td")));
        return searchResultsElements;
    }

    @Deprecated
    private static boolean clickElementInList(String value, String componentId, String wrapperClassName, boolean exactMatch) {
        return clickElementInList(new String[] {value}, componentId, wrapperClassName, exactMatch);
    }

}
