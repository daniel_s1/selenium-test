package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.edge;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import uk.ac.cam.automation.seleniumframework.driver.DriverCreationException;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;
import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

public class EdgeLocalWebDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {

        if (System.getProperty("webdriver.edge.driver") == null) {
            String edgeDriver = PropertyLoader.getProperty(CommonProperties.SELENIUM_DRIVER_PATH_EDGE);
            if (edgeDriver == null) {
                throw new DriverCreationException("You have specified Edge Local as your browser but have not set the " + CommonProperties.SELENIUM_DRIVER_PATH_EDGE + " property either as a command line argument or in a registered properties file.");
            }

            System.setProperty("webdriver.edge.driver", edgeDriver);
        }
        return new EdgeDriver();
    }

}