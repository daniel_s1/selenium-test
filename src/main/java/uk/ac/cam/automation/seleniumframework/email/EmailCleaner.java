package uk.ac.cam.automation.seleniumframework.email;

import uk.ac.cam.automation.seleniumframework.log.Log;

import javax.mail.*;

public class EmailCleaner extends BaseEmailService {

    /**
     * Deletes all messages for a given email address, if the account does not exist then a warning will be put on the log. If anything else goes wrong a MailDeletionException is thrown
     */
    public static void removeAllMessages(String emailAddress) throws MailDeletionException {

        try{
            Store store = openMailClient();

            store.connect(mailHost, emailAddress, emailAddress);
            InboxFolder inbox = new InboxFolder(store.getFolder("INBOX"));
            inbox.getInbox().open(Folder.READ_WRITE);

            for(Message message : inbox.getInbox().getMessages()) {
                message.setFlag(Flags.Flag.DELETED, true);
            }

            inbox.getInbox().expunge();

            inbox.getInbox().close(true);

        } catch (AuthenticationFailedException e) {
            Log.Warn("Could not authenticate using account " + emailAddress + " from host: " + mailHost + " on port: " + port + ". It is likely that this account does not yet exist: " + e.getMessage());
        } catch (MessagingException e) {
            throw new MailDeletionException("Could not delete messages from account " + emailAddress + " from host: " + mailHost + " on port: " + port + ": " + e.getMessage());
        }
    }





}