package uk.ac.cam.automation.seleniumframework.email.service;

import uk.ac.cam.automation.seleniumframework.email.EmailFailedException;
import uk.ac.cam.automation.seleniumframework.email.domain.Email;

public interface EmailService {
    void sendEmailAtomically(Email var1) throws EmailFailedException;
}

