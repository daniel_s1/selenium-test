package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.ie;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import uk.ac.cam.automation.seleniumframework.driver.DriverCreationException;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;
import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

public class IELocalWebDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {

        if (System.getProperty("webdriver.ie.driver") == null) {
            String ieDriver = PropertyLoader.getProperty(CommonProperties.SELENIUM_DRIVER_PATH_IE);
            if (ieDriver == null) {
                throw new DriverCreationException("You have specified IE Local as your browser but have not set the " + CommonProperties.SELENIUM_DRIVER_PATH_IE + " property either as a command line argument or in a registered properties file.");
            }
            System.setProperty("webdriver.ie.driver", ieDriver);
        }
        InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
        internetExplorerOptions.ignoreZoomSettings();
        internetExplorerOptions.requireWindowFocus();
        internetExplorerOptions.destructivelyEnsureCleanSession();
        return new InternetExplorerDriver(internetExplorerOptions);
    }

}