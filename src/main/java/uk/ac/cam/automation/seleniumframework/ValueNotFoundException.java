package uk.ac.cam.automation.seleniumframework;

public class ValueNotFoundException extends RuntimeException {

    public ValueNotFoundException(String message) {
        super(message);
    }

}
